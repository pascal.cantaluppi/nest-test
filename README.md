# NestJS Test App (API)

<p align="center">
  <img src="https://nestjs.com/img/logo_text.svg" width="200" alt="Nest Logo" /></a>
</p>

<p align="center">A progressive Node.js framework for building efficient and scalable server-side applications, heavily inspired by Angular.</p>

<a href="https://www.academind.com/learn/node-js/nestjs-introduction/" target="blank">https://www.academind.com/learn/node-js/nestjs-introduction/</a>
