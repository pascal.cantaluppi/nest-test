import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Patch,
  Delete
} from "@nestjs/common";
import { ProductService } from "./products.service";

@Controller("products")
export class ProductsController {
  constructor(private readonly productsService: ProductService) {}
  @Post()
  async addProduct(
    @Body("title") prodTitle: string,
    @Body("description") prodDescription: string,
    @Body("price") prodPrice: number
  ) {
    const generatedId = await this.productsService.insertProduct(
      prodTitle,
      prodDescription,
      prodPrice
    );
    return { id: generatedId };
  }

  @Get()
  async getAllProducts() {
    const products = await this.productsService.getProducts();
    return products.map(prod => ({
      id: prod.id,
      title: prod.title,
      description: prod.description,
      price: prod.price
    }));
  }

  @Get(":id")
  async getProduct(@Param("id") productId: string) {
    return await this.productsService.getProduct(productId);
  }

  @Patch(":id")
  async updateProduct(
    @Param("id") productId: string,
    @Body("title") prodTitle: string,
    @Body("description") prodDescription: string,
    @Body("price") prodPrice: number
  ) {
    await this.productsService.updateProduct(
      productId,
      prodTitle,
      prodDescription,
      prodPrice
    );
    return null;
  }

  @Delete(":id")
  async deleteProduct(@Param("id") productId: string) {
    await this.productsService.deleteProduct(productId);
  }
}
