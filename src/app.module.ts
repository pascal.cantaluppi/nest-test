import { Module } from "@nestjs/common";
import { ProductsModule } from "./products/products.module";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { MongooseModule } from "@nestjs/mongoose";
require("dotenv").config(); // TODO: Use Nest.ja .env Implementation

@Module({
  imports: [
    ProductsModule,
    MongooseModule.forRoot(
      "mongodb+srv://pascal:" +
        process.env.SECRET +
        "@nest-vgbqx.azure.mongodb.net/nest-test?retryWrites=true&w=majority"
    )
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
